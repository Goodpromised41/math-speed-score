using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameOver : MonoBehaviour
{
    public Text outputtext;
    private ScoreManager scoreManager;
    void Start()
    {
        outputtext.text = ScoreManager.Instance.ReResult().ToString();
    }

    public void StartGame()
    {
        SceneManager.LoadScene("SampleScene");
    }

    public void PortStartGame()
    {
        SceneManager.LoadScene("SampleScenePort");
    }

    public void Back()
    {
        SceneManager.LoadScene("MainMenu");
    }

    public void PortBack()
    {
        SceneManager.LoadScene("MainMenuPort");
    }
}
