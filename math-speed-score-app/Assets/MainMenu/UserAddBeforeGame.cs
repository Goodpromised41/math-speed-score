using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UserAddBeforeGame : MonoBehaviour
{
    public InputField addUserNameField;
    public Text hintText;
    public GameObject mainTable;
    public int orientetion;

    public void addUserButton()
    {
        if (addUserNameField.text.Length == 0)
        {
            hintText.text = "���� � ������ ������";
            return;
        }

        if (addUserNameField.text.Length > 30)
        {
            hintText.text = "��� ������ ���� ������ 30 ��������";
            return;
        }

        if (mainTable.GetComponent<UserTable>().isExistsUserName(addUserNameField.text))
        {
            hintText.text = "��� ��� ����������";
            return;
        }

        mainTable.GetComponent<UserTable>().addUser(addUserNameField.text);
        addUserNameField.text = "";
        if (orientetion == 1)
        {
            SceneManager.LoadScene("LevelsExample");
        }
        if (orientetion == 2)
        {
            SceneManager.LoadScene("PortLevels");
        }        
    }
}
