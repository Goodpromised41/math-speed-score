using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuButtonAction : MonoBehaviour
{
    public GameObject addUserOption;
    public UserTable userTable;

    public void ExitGame()
    {
        Application.Quit();
    }

    public void StartGame()
    {
        userTable.loadSelectUser();
        if (UserTable.selectUser != null)
        {
            SceneManager.LoadScene("LevelsExample");
        }
        else
        {
            addUserOption.SetActive(true);
        }
    }


    public void PortStartGame()
    {
        userTable.loadSelectUser();
        if (UserTable.selectUser != null)
        {
            SceneManager.LoadScene("PortLevelsExample 1");
        }
        else
        {
            addUserOption.SetActive(true);
        }
        
    }

    public void GamerResult()
    {
        SceneManager.LoadScene("GamerResult");
    }

    public void PortGamerResult()
    {
        SceneManager.LoadScene("PortGamerResult");
    }

    public void OrientetionChanged()
    {
        SceneManager.LoadScene("MainMenuPort");
    }

    public void GamerAchievement()
    {
        SceneManager.LoadScene("Achievement");
    }

    public void PortGamerAchievement()
    {
        SceneManager.LoadScene("PortAchievement");
    }
}
