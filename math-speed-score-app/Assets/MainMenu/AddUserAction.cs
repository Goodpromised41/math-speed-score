using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AddUserAction : MonoBehaviour
{
    public InputField addUserNameField;
    public Text hintText;
    public GameObject mainTable;

    public void addUserButton ()
    {
        if (addUserNameField.text.Length == 0)
        {
            hintText.text = "���� � ������ ������";
            return;
        }

        if (addUserNameField.text.Length > 30)
        {
            hintText.text = "��� ������ ���� ������ 30 ��������";
            return;
        }

        if (mainTable.GetComponent<UserTable>().isExistsUserName (addUserNameField.text))
        {
            hintText.text = "��� ��� ����������";
            return;
        }

        mainTable.GetComponent<UserTable>().addUser(addUserNameField.text);
        addUserNameField.text = "";
    }
}
