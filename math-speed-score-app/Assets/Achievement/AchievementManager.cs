using UnityEngine;
using System.Collections.Generic;

public class AchievementManager : MonoBehaviour
{
    public delegate void AchievementUnlockedHandler(Achievement achievement);
    public event AchievementUnlockedHandler OnAchievementUnlocked;

    private List<Achievement> achievements;
    public AchievementDatabase achievementDatabase;

    private void Start()
    {
        achievements = new List<Achievement>();
    }

    public void AddAchievementInBase(Achievement achievement)
    {
            achievementDatabase.AddAchievement(achievement);
    }
    public void AddAchievement(Achievement achievement)
    {
        achievements.Add(achievement);
    }

    public void MarkAchievementAsUnlocked(Achievement achievement)
    {
        achievement.isUnlocked = true;
        achievementDatabase.UpdateAchievement(achievement);
        OnAchievementUnlocked?.Invoke(achievement);
    }

    public bool IsAchievementAdded(Achievement achievement)
    {
        if (achievementDatabase.IsAchievementUnlock(achievement))  return true;
        else return false;
    }

    public Achievement[] GetAchievements()
    {
        return achievements.ToArray();
    }
}