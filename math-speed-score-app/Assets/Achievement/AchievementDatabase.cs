using UnityEngine;
using UnityEngine.UI;
using System.Data;
using Mono.Data.Sqlite;
using System.Collections.Generic;
using System;
using System.IO;

public class AchievementDatabase : MonoBehaviour
{
    private string connectionString;
    private const string fileName = "db.bytes";

    public AchievementManager achievementManager;

    private void Start()
    {
        connectionString = "Data Source=" + GetDatabasePath();

        CreateAchievementsTable();

         

    }

    private string GetDatabasePath()
    {
    #if UNITY_EDITOR
            return Path.Combine(Application.streamingAssetsPath, fileName);
    #elif UNITY_STANDALONE
                string filePath = Path.Combine(Application.dataPath, fileName);
                if(!File.Exists(filePath)) UnpackDatabase(filePath);
                return filePath;
    #elif UNITY_ANDROID
                string filePath = Path.Combine(Application.persistentDataPath, fileName);
                if (!File.Exists(filePath)) UnpackDatabase(filePath);
                return filePath;
    #endif
    }

    private void UnpackDatabase(string toPath)
    {
        string fromPath = Path.Combine(Application.streamingAssetsPath, fileName);
        WWW reader = new WWW(fromPath);
        while (!reader.isDone) { }

        File.WriteAllBytes(toPath, reader.bytes);
    }

    private void CreateAchievementsTable()
    {
        if (!TableExists("achievements"))
        {
            try
            {
                using (var connection = new SqliteConnection(connectionString))
                {
                    connection.Open();

                    string query = "CREATE TABLE IF NOT EXISTS achievements (id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT NOT NULL UNIQUE, description TEXT NOT NULL UNIQUE, unlocked INTEGER, image BLOB)";
                    using (var command = new SqliteCommand(query, connection))
                    {
                        command.ExecuteNonQuery();
                    }

                    foreach (var item in achievementManager.GetAchievements())
                        AddAchievement(item);

                    connection.Close();
                }
            }
            catch(Exception ex) { new Exception(ex.Message); }
        }
    }
    private bool TableExists(string tableName)
    {
        using (var connection = new SqliteConnection(connectionString))
        {
            connection.Open();

            var cmd = connection.CreateCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT name FROM sqlite_master WHERE type='table' AND name=@tableName";
            cmd.Parameters.AddWithValue("@tableName", tableName);

            using (var reader = cmd.ExecuteReader())
            {
                bool tableExists = reader.HasRows;
                connection.Close();
                return tableExists;
            }
        }
    }

    public void AddAchievement(Achievement achievement)
    {
        using (var connection = new SqliteConnection(connectionString))
        {
            connection.Open();
            Texture2D newtex = UncompressTexture(achievement.image);
            byte[] imageBytes = newtex.EncodeToPNG();

            string query = "INSERT INTO achievements (name, description, unlocked, image) VALUES (@name, @description, @unlocked, @image)";
            try
            {
                using (var command = new SqliteCommand(query, connection))
                {
                    command.Parameters.AddWithValue("@name", achievement.name);
                    command.Parameters.AddWithValue("@description", achievement.description);
                    command.Parameters.AddWithValue("@unlocked", achievement.isUnlocked ? 1 : 0);
                    command.Parameters.AddWithValue("@image", imageBytes);

                    command.ExecuteNonQuery();
                }
                connection.Close();
            }
            catch (Exception ex) { new Exception(ex.Message); }
        }
    }

    public bool IsAchievementUnlock(Achievement achievement)
    {
        using (var connection = new SqliteConnection(connectionString))
        {
            connection.Open();

            string query = "SELECT unlocked FROM achievements WHERE id = @id";
            using (var command = new SqliteCommand(query, connection))
            {
                command.Parameters.AddWithValue("@id", achievement.id);
                using (var reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        bool isUnlocked = reader.GetBoolean(0);
                        connection.Close();
                        return isUnlocked;
                    }
                }
            }
        }

        return achievement.isUnlocked;
    }
  
    public Achievement[] GetAchievements()
    {
        using (var connection = new SqliteConnection(connectionString))
        {
            connection.Open();

            string query = "SELECT * FROM achievements";
            using (var command = new SqliteCommand(query, connection))
            {
                using (var reader = command.ExecuteReader())
                {
                    var achievements = new List<Achievement>();
                    while (reader.Read())
                    {
                        int id = reader.GetInt32(0);
                        string name = reader.GetString(1);
                        string description = reader.GetString(2);
                        bool unlocked = reader.GetInt32(3) == 1;
                        byte[] imageBytes = GetImageFromReader(reader, 4);

                        Texture2D texture = new Texture2D(1,1);
                        texture.LoadImage(imageBytes);
                        Achievement achievement = new Achievement(id, name, description, unlocked, texture);
                        achievements.Add(achievement);
                    }

                    connection.Close();

                    return achievements.ToArray();
                }
            }
        }
    }

    public void UpdateAchievement(Achievement achievement)
    {
        using (var connection = new SqliteConnection(connectionString))
        {
            connection.Open();

            string query = "UPDATE achievements SET unlocked = @unlocked WHERE id = @id";
            using (var command = new SqliteCommand(query, connection))
            {
                command.Parameters.AddWithValue("@unlocked", achievement.isUnlocked ? 1 : 0);
                command.Parameters.AddWithValue("@id", achievement.id);

                command.ExecuteNonQuery();
            }

            connection.Close();
        }
    }
    public Texture2D UncompressTexture(Texture2D compressedTexture)
    {
        Texture2D uncompressedTexture = new Texture2D(compressedTexture.width, compressedTexture.height, TextureFormat.RGBA32, false);
        uncompressedTexture.SetPixels(compressedTexture.GetPixels());
        uncompressedTexture.Apply();

        return uncompressedTexture;
    }

    public void AddImageToAchievement(Achievement achievement, byte[] imageBytes)
    {
        using (var connection = new SqliteConnection(connectionString))
        {
            connection.Open();
            string query = "UPDATE achievements SET image = @image WHERE id = @id";
            using (var command = new SqliteCommand(query, connection))
            {
                command.Parameters.AddWithValue("@image", imageBytes);
                command.Parameters.AddWithValue("@id", achievement.id);

                command.ExecuteNonQuery();
            }

            connection.Close();
        }
    }

    private byte[] GetImageFromReader(SqliteDataReader reader, int columnIndex)
     {
         if (!reader.IsDBNull(columnIndex))
         {
             long byteLength = reader.GetBytes(columnIndex, 0, null, 0, 0);
             byte[] buffer = new byte[byteLength];
             reader.GetBytes(columnIndex, 0, buffer, 0, (int)byteLength);
             return buffer;
         }
         else
         {
             return null;
         }
     }

     public static Texture2D BytesToTexture(byte[] bytes)
     {
         Texture2D texture = new Texture2D(1, 1);
         texture.LoadImage(bytes);
         return texture;
     }

     public static Sprite TextureToSprite(Texture2D texture)
     {
         Rect rect = new Rect(0, 0, texture.width, texture.height);
         Vector2 pivot = new Vector2(0.5f, 0.5f);
         Sprite sprite = Sprite.Create(texture, rect, pivot);
         return sprite;
     }
     public Sprite GetAchievementImage(int achievementId)
     {
         using (var connection = new SqliteConnection(connectionString))
         {
             connection.Open();

             string query = "SELECT image FROM achievements WHERE id = @id";
             using (var command = new SqliteCommand(query, connection))
             {
                 command.Parameters.AddWithValue("@id", achievementId);

                 using (var reader = command.ExecuteReader())
                 {
                     if (reader.Read())
                     {
                         byte[] imageBytes = (byte[])reader["image"];
                         Texture2D texture = new Texture2D(1, 1);
                         texture.LoadImage(imageBytes);

                         Sprite sprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), Vector2.one * 0.5f);

                         connection.Close();

                         return sprite;
                     }
                 }
             }

             connection.Close();
         }

         return null;
     }

}