using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class AchievementController : MonoBehaviour
{
    public delegate void AchievementNotificationHandler(Achievement achievement);
    public event AchievementNotificationHandler OnAchievementNotification;
    public AchievementManager achievementManager;
    private int score;
    float startTime;
    public static int newresult = 0;
    

    public static bool checkneed = false;
    public static bool newcloudspawn = false;

    public Achievement achievement1;
    public Achievement achievement2;
    public Achievement achievement3;
    public Achievement achievement4;
    public Achievement achievement5;
    public Achievement achievement6;
    public Achievement achievement7;
    public Achievement achievement8;
    public Achievement achievement9;
    public Achievement achievement10;

    private void Start()
    {
        achievementManager.OnAchievementUnlocked += HandleAchievementUnlocked;
        float startTime = Time.time;
        achievementManager.AddAchievementInBase(achievement1);
        achievementManager.AddAchievementInBase(achievement2);
        achievementManager.AddAchievementInBase(achievement3);
        achievementManager.AddAchievementInBase(achievement4);
        achievementManager.AddAchievementInBase(achievement5);
        achievementManager.AddAchievementInBase(achievement6);
        achievementManager.AddAchievementInBase(achievement7);
        achievementManager.AddAchievementInBase(achievement8);
        achievementManager.AddAchievementInBase(achievement9);
        achievementManager.AddAchievementInBase(achievement10);
    }

    private void Update()
    {
        score = ScoreManager.Instance.ReResult();
        if (newcloudspawn)
        {
            startTime = Time.time;
            newcloudspawn = false;
        }
        
        if (checkneed)
        {
            CheckAchievements();
        }
    }

    public void CheckAchievements()
    {
        float solutionTime = Time.time - startTime;
        if (score >= 1 && !achievementManager.IsAchievementAdded(achievement1))
        {
            achievement1.isUnlocked = true;
            achievementManager.AddAchievement(achievement1);
            achievementManager.MarkAchievementAsUnlocked(achievement1);
        }

        if (score >= 10 && RandomSpawn.difficulty == 1 && !achievementManager.IsAchievementAdded(achievement2))
        {
            achievement2.isUnlocked = true;
            achievementManager.AddAchievement(achievement2);
            achievementManager.MarkAchievementAsUnlocked(achievement2);
        }
        if (score >= 10 && RandomSpawn.difficulty == 2 &&!achievementManager.IsAchievementAdded(achievement3))
        {
            achievement3.isUnlocked = true;
            achievementManager.AddAchievement(achievement3);
            achievementManager.MarkAchievementAsUnlocked(achievement3);
        }

        if (score >= 10 && RandomSpawn.difficulty == 3 && !achievementManager.IsAchievementAdded(achievement4))
        {
            achievement4.isUnlocked = true;
            achievementManager.AddAchievement(achievement4);
            achievementManager.MarkAchievementAsUnlocked(achievement4);
        }
        if (score >= 100 && RandomSpawn.difficulty == 1 && !achievementManager.IsAchievementAdded(achievement5))
        {
            achievement5.isUnlocked = true;
            achievementManager.AddAchievement(achievement5);
            achievementManager.MarkAchievementAsUnlocked(achievement5);
        }
        if (score >= 50 && RandomSpawn.difficulty == 2 && !achievementManager.IsAchievementAdded(achievement6))
        {
            achievement6.isUnlocked = true;
            achievementManager.AddAchievement(achievement6);
            achievementManager.MarkAchievementAsUnlocked(achievement6);
        }
        if (score >= 100 && RandomSpawn.difficulty == 3 && !achievementManager.IsAchievementAdded(achievement7))
        {
            achievement7.isUnlocked = true;
            achievementManager.AddAchievement(achievement7);
            achievementManager.MarkAchievementAsUnlocked(achievement7);
        }
        if (TextVeiw.newtext.Equals(newresult.ToString()) && solutionTime <= 2 && !achievementManager.IsAchievementAdded(achievement8))
        {
            achievement8.isUnlocked = true;
            achievementManager.AddAchievement(achievement8);
            achievementManager.MarkAchievementAsUnlocked(achievement8);
        }
        if (score >= 10000 && !achievementManager.IsAchievementAdded(achievement9))
        {
            achievement9.isUnlocked = true;
            achievementManager.AddAchievement(achievement9);
            achievementManager.MarkAchievementAsUnlocked(achievement9);
        }
        if (score >= 100 && RandomSpawn.mode == "All" && !achievementManager.IsAchievementAdded(achievement10))
        {
            achievement10.isUnlocked = true;
            achievementManager.AddAchievement(achievement10);
            achievementManager.MarkAchievementAsUnlocked(achievement10);
        }
        checkneed = false;
    }
    private void HandleAchievementUnlocked(Achievement unlockedAchievement)
    {
        ShowAchievementNotification(unlockedAchievement);
    }

    private void ShowAchievementNotification(Achievement achievement)
    {
        OnAchievementNotification?.Invoke(achievement);
    }
}