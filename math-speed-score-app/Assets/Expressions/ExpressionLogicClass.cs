using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExpressionLogicClass : MonoBehaviour
{
    private int MinPlusMinusNumber,MaxPlusMinusNumber,MaxMultiplyNumber;
    private string expr_operator;
    private System.Random random;
    private int first_number, second_number, result;

    private int difficulty;

    public static List<string> operators = new List<string>();


    public void SetBoundariesForNumners(int _difficulty)
    {   
        switch (_difficulty)
        {
            case 1:
            {
                MaxPlusMinusNumber = 10;
                MinPlusMinusNumber = 1;
                MaxMultiplyNumber = 4;
                break;
            }
            case 2:
            {
                MaxPlusMinusNumber = 25;
                MinPlusMinusNumber = 5;
                MaxMultiplyNumber = 7;
                break;
            }
            case 3:
            {
                MaxPlusMinusNumber = 50;
                MinPlusMinusNumber = 10;
                MaxMultiplyNumber = 12;
                break;
            }
            case 4:
            {
                MaxPlusMinusNumber = 80;
                MinPlusMinusNumber = 40;
                MaxMultiplyNumber = 16;
                break;
            }
            default: break;
        }   
    }
  
    public ExpressionLogicClass()
    {
        difficulty = 1;
        SetBoundariesForNumners(difficulty);
        random = new System.Random();
    }
    public ExpressionLogicClass(int _difficulty)
    {
        difficulty = _difficulty;
        SetBoundariesForNumners(difficulty);
        random = new System.Random();
    }   
    public string ReturnStringExpression()
    {
        return first_number + expr_operator + second_number;
    }
    public int ReturnExpressionResult()
    {
        CalculateResult(first_number,second_number,expr_operator);
        return result;
    }
    public void CreateExpression()
    {
        Debug.Log(operators.Count);
        expr_operator = operators[random.Next(0, operators.Count)];   
        GetFirstNumber(expr_operator);
        GetSecondNumber(expr_operator,first_number);
    }
    private void GetFirstNumber(string expr_operator) 
    {
        if (expr_operator.Equals("*"))
        {
            first_number = random.Next(2, MaxMultiplyNumber);
        }
        else if(expr_operator.Equals("/"))
        {
            first_number = random.Next(2, MaxPlusMinusNumber);
            while (first_number % 2 != 0)
            {
                first_number = random.Next(2, MaxPlusMinusNumber);
            }
        }
        else first_number = random.Next(MinPlusMinusNumber, MaxPlusMinusNumber);
    }
    private void GetSecondNumber(string expr_operator, int first_number)
    {
        if (expr_operator.Equals("/"))
        {
            second_number = random.Next(2, first_number);
            while (first_number % second_number != 0) 
            {
                second_number = random.Next(2, first_number);
            }
        }
        else if (expr_operator.Equals("*")) 
        {
           second_number = random.Next(2, MaxMultiplyNumber);
        }   
        else
        {   
            second_number = random.Next(MinPlusMinusNumber, MaxPlusMinusNumber);
            if(difficulty < 3){
                while(first_number <= second_number)
                {
                    second_number = random.Next(MinPlusMinusNumber, MaxPlusMinusNumber);
                }
            }
        }
    }
    private bool CheckOnSimpleNumber(int number) 
    {
        List<int> simple_numbers = new List<int>(){2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97 };
        if (simple_numbers.Contains(number)) 
        {
            return true;
        }
        return false;
    }
    private void CalculateResult(int operand1, int operand2, string operatorSymbol)
    {
        switch (operatorSymbol)
        {
            case "+":
                {
                result = operand1 + operand2;
                break;
                }
            case "-":
                {
                result = operand1 - operand2;
                break;
                }
            case "*":
                {
                result = operand1 * operand2;
                break;
                }
            case "/":
                {
                result = operand1 / operand2;
                break;
                }
        }  
    }
}
