using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class LevelsChanged : MonoBehaviour
{
    public Text difficulty_text;
    public Text plus_text;
    public Text minus_text;
    public Text multiply_text;
    public Text divide_text;
    public Toggle toggle;
    int difficulty;

    List<string> _operators;

    public void Start()
    {
        RandomSpawn.mode = "All";
        RandomSpawn.difficulty = 1;
        RandomSpawn.acceleration = true;
        difficulty = 1;

        _operators = new List<string>();
    }

    public void PlusButton(bool act)
    {
        if(!_operators.Contains("+"))
        {    
            if(act)
            {
                _operators.Add("+");
                plus_text.color = new Color(0,255,0,1);
            }
        }
        if(!act)
        {
            _operators.Remove("+");
            plus_text.color = new Color(255,0,0,1);
        }
    }
    public void MinusButton(bool act)
    {
        if(!_operators.Contains("-"))
        {    
            if(act)
            {
                _operators.Add("-");
                minus_text.color = new Color(0,255,0,1);
            }
        }
        if(!act)
        {
            _operators.Remove("-");
            minus_text.color = new Color(255,0,0,1);
        }

    }
    public void MultButton(bool act)
    {
        if(!_operators.Contains("*"))
        {    
            if(act)
            {
                _operators.Add("*");
                multiply_text.color = new Color(0,255,0,1);
            }
        }
        if(!act)
        {
            _operators.Remove("*");
            multiply_text.color = new Color(255,0,0,1);
        }
    }
    public void DivButton(bool act)
    {
        if(!_operators.Contains("/"))
        {    
            if(act)
            {
                _operators.Add("/");
                divide_text.color = new Color(0,255,0,1);
            }
        }
        if(!act)
        {
            _operators.Remove("/");
            divide_text.color = new Color(255,0,0,1);
        }
    }



    public void InputLevels123(int value)
    {
        if (value == 0)
        {
            RandomSpawn.mode = "All"; 
        }
        if (value == 1)
        {
            RandomSpawn.mode = "Summ only";
        }
        if (value == 2)
        {
            RandomSpawn.mode = "Diff only";
        }
        if (value == 3)
        {
            RandomSpawn.mode = "Mult only";
        }
        if (value == 4)
        {
            RandomSpawn.mode = "Div only";
        }
    }

    public void InputDifficulty(int value)
    {
        if (value == 0)
        {
            RandomSpawn.difficulty = 1; 
        }
        if (value == 1)
        {
            RandomSpawn.difficulty = 2;
        }
        if (value == 2)
        {
            RandomSpawn.difficulty = 3;
        }
    }

    public void OnToggleClick()
    {
        if (toggle.isOn == true)
        {
            RandomSpawn.acceleration = true;
        }
        else
        {
            RandomSpawn.acceleration = false;
        }
    }
    public void UpdateText(int difficulty)
    {     
        switch(difficulty)
        {
            case 1:
            {
                difficulty_text.text = "Легкий"; 
                break;
            }
             case 2:
            {
                difficulty_text.text = "Средний"; 
                break;
            }
             case 3:
            {
                difficulty_text.text = "Сложный"; 
                break;
            }
            default: break;
        }
    }
    public void RightButton()
    {
        if(difficulty!=3){
            difficulty = UnityEngine.Mathf.Min(3, difficulty+1);
            RandomSpawn.difficulty = difficulty;
            UpdateText(difficulty);
        }
        else
        {
            difficulty = 1;
            RandomSpawn.difficulty = difficulty;
            UpdateText(difficulty);
        }
    }

    public void LeftButton()
    {
        if(difficulty!=1){
            difficulty = UnityEngine.Mathf.Max(0, difficulty-1);
            RandomSpawn.difficulty = difficulty;
            UpdateText(difficulty);
        }
        else
        {
            difficulty = 3;
            RandomSpawn.difficulty = difficulty;
            UpdateText(difficulty);
        }      
    }

    public void StartGame()
    {
        if(_operators.Count > 0)
        {
            ExpressionLogicClass.operators = _operators;
            SceneManager.LoadScene("SampleScene");
        }
        
    }

    public void PortStartGame()
    {
        if (_operators.Count > 0)
        {
            ExpressionLogicClass.operators = _operators;
            SceneManager.LoadScene("SampleScene23");
        }
        
    }

    public void Back()
    {
        SceneManager.LoadScene("MainMenu");
    }

    public void PortBack()
    {
        SceneManager.LoadScene("MainMenuPort");
    }
}
