using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Cloud : MonoBehaviour
{
    public float translateSpeed;
    private string problem;
    private int result;

    public void SetSpeed(float speed)
    {
        translateSpeed = speed;
    }
    public void SetProblem(string problem, int result)
    {
        this.problem = problem;
        this.result = result;
    }
    private void Update()
    {
        transform.Translate(Vector2.down * translateSpeed * Time.deltaTime);
    }
    public int ReResult()
    {
        return result;
    }
    public abstract void OnTriggerEnter2D(Collider2D collision);
}