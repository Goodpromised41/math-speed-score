using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthCloud : Cloud
{
    public override void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Destr"))
        {
            Destroy(gameObject);
            LivesManager.Instance.DecreaseLives();
        }
    }
}
