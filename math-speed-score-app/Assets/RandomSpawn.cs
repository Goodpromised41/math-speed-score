using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomSpawn : MonoBehaviour
{
    [SerializeField]
    private GameObject cloudPrefab;
    [SerializeField]
    private GameObject bonusCloudPrefab;
    [SerializeField]
    private GameObject healthCloudPrefab;
    [SerializeField]
    private GameObject numberPrefab;
    [SerializeField]
    private Transform numberParent;
    [SerializeField]
    private float spawnRate = 5.0f;
    public static bool acceleration = true;
    private float nextAccelerationTime = 0.0f;
    public static string mode = "All";
    public static int difficulty = 3;
    [SerializeField]
    public int orientationSpawn;

    Vector2 whereToSpawn;
    float RandX = 0.0f;
    private float nextSpawn = 0.0f;
    
    private bool IsDiffucultyIncreaseNeeded = false;
    string cloudType = "";

    private float overlapRadius = 2f;
    
    private void Start()
    {
        SetCloudsSpeed();
    }
    private void Update()
    {
        if (acceleration && Time.time > nextAccelerationTime)
        {
            IncreaseCloudsSpeed();
        }
        if (Time.time > nextSpawn)
        {
            nextSpawn = Time.time + spawnRate;
            SpawnCloud();
        }
    }
    private void SetCloudsSpeed()
    {
        switch (difficulty)
        {
            case 1:
            {
                //Cloud.translateSpeed = 30.0f; 
                spawnRate = spawnRate;
                break;
            }
            case 2:
            {
                //Cloud.translateSpeed = 30.0f;
                spawnRate = spawnRate * 35 / 40;
                break;
            }
            case 3:
            {
                //Cloud.translateSpeed = 30.0f;
                spawnRate = spawnRate * 3 / 4;
                break;
            }
            default: break;
        }
    }
    private void IncreaseCloudsSpeed()
    {
        switch (difficulty)
        {
            case 1:
            {
                //Cloud.translateSpeed += 2.5f;
                nextAccelerationTime = Time.time + 20;
                spawnRate -= 0.1f;
                break;
            }
            case 2:
            {
                //Cloud.translateSpeed += 1.5f;
                nextAccelerationTime = Time.time + 20;
                spawnRate -= 0.075f;
                break;
            }
            case 3: 
            {
                //Cloud.translateSpeed += 0.5f;
                nextAccelerationTime = Time.time + 20;
                spawnRate -= 0.05f;
                break;
            }
            default: break;
        }
    }

    private void PortIncreaseCloudsSpeed()
    {
        switch (difficulty)
        {
            case 1:
                {
                    //Cloud.translateSpeed += 2.5f;
                    nextAccelerationTime = Time.time + 20;
                    spawnRate -= 0.1f;
                    break;
                }
            case 2:
                {
                    //Cloud.translateSpeed += 1.5f;
                    nextAccelerationTime = Time.time + 20;
                    spawnRate -= 0.075f;
                    break;
                }
            case 3:
                {
                    //Cloud.translateSpeed += 0.5f;
                    nextAccelerationTime = Time.time + 20;
                    spawnRate -= 0.05f;
                    break;
                }
            default: break;
        }
    }
    

    private GameObject GetCloudTypeForSpawn()
    {
        GameObject cloud = null;

        System.Random random = new System.Random();
        int number = random.Next(101);
        if(number > 20)
        {
            //simple cloud
            cloudType = "Simple";
            IsDiffucultyIncreaseNeeded = false;
            cloud = Instantiate(cloudPrefab,whereToSpawn,Quaternion.identity);
            return cloud;
        }
        else
        {
            int randomChoose = random.Next(1,3);
            IsDiffucultyIncreaseNeeded = true; //Увеличим сложность уравнения для редких бонусных облаков.
            switch (randomChoose)
            {
                case 1: 
                {                
                    //Golden cloud
                    cloudType = "Golden";
                    cloud = Instantiate(bonusCloudPrefab, whereToSpawn, Quaternion.identity);
                    return cloud;
                    break;
                }
                case 2:
                {        
                    //Health cloud
                    cloudType = "Health";
                    cloud = Instantiate(healthCloudPrefab, whereToSpawn, Quaternion.identity);       
                    return cloud;
                    break;
                }
                default: break;
            }
            
        }
        return cloud;
    }

    private void SetCloudData(GameObject _cloud, string _expression, int _result)
    {
        switch(cloudType)
        {
            case "Simple":
            {
                _cloud.GetComponent<SimpleCloud>().SetProblem(_expression, _result); 
                _cloud.GetComponent<SimpleCloud>().SetSpeed(30.0f);
                break;              
            }
            case "Golden":
            {
                _cloud.GetComponent<GoldenCloud>().SetProblem(_expression, _result);  
                _cloud.GetComponent<GoldenCloud>().SetSpeed(80.0f);
                break;    
            }
            case "Health":
            {
                _cloud.GetComponent<HealthCloud>().SetProblem(_expression, _result);   
                _cloud.GetComponent<HealthCloud>().SetSpeed(80.0f);
                break;    
            }
            default: break;
        }
        _cloud.GetComponent<TextMesh>().text = _expression;
    }
    private void SpawnCloud()
    {
        if (orientationSpawn == 1)
        {
            RandX = UnityEngine.Random.Range(-600f, 600);
        }
        if (orientationSpawn == 2)
        {
            RandX = UnityEngine.Random.Range(-200f, 200);
        }
        
        whereToSpawn = new Vector2(RandX, transform.position.y);
        Collider2D[] colliders = Physics2D.OverlapCircleAll(whereToSpawn, overlapRadius);
        if (colliders.Length > 0)
        {
            return;
        }
        GameObject cloud = GetCloudTypeForSpawn();
        ExpressionLogicClass logic = new ExpressionLogicClass(IsDiffucultyIncreaseNeeded ? difficulty + 1 : difficulty);
        logic.CreateExpression();
        string expression = logic.ReturnStringExpression();
        int result = logic.ReturnExpressionResult();

        SetCloudData(cloud, expression, result);
    }

    private void PortSpawnCloud()
    {
        RandX = UnityEngine.Random.Range(-200f, 200);
        whereToSpawn = new Vector2(RandX, transform.position.y);
        Collider2D[] colliders = Physics2D.OverlapCircleAll(whereToSpawn, overlapRadius);
        if (colliders.Length > 0)
        {
            return;
        }
        GameObject cloud = Instantiate(cloudPrefab, whereToSpawn, Quaternion.identity);
        ExpressionLogicClass logic = new ExpressionLogicClass(IsDiffucultyIncreaseNeeded ? difficulty + 1 : difficulty);
        logic.CreateExpression();
        string expression = logic.ReturnStringExpression();
        int result = logic.ReturnExpressionResult();

        SetCloudData(cloud, expression, result);
    }

}