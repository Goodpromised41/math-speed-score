using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextVeiw : MonoBehaviour
{
    public static int result;
    public Text text;
    public static string newtext;

    private void Start()
    {
        
    }
    private void Update()
    {
         // Вызываем обработчик кнопки "0"
        if( Input.GetKeyDown( KeyCode.Keypad0) || Input.GetKeyDown(KeyCode.Alpha0) ) 
        {
            ButClick("0");            
        }
        // Вызываем обработчик кнопки "1"
        if( Input.GetKeyDown(KeyCode.Keypad1) || Input.GetKeyDown(KeyCode.Alpha1)) 
        {
            ButClick("1");        }
        // Вызываем обработчик кнопки "2"
        if( Input.GetKeyDown(KeyCode.Keypad2) || Input.GetKeyDown(KeyCode.Alpha2)) 
        {
            ButClick("2");        }
        // Вызываем обработчик кнопки "3"
        if( Input.GetKeyDown(KeyCode.Keypad3) || Input.GetKeyDown(KeyCode.Alpha3)) 
        {
            ButClick("3");        }
        // Вызываем обработчик кнопки "4"
        if( Input.GetKeyDown(KeyCode.Keypad4) || Input.GetKeyDown(KeyCode.Alpha4)) 
        {
            ButClick("4");        }
        // Вызываем обработчик кнопки "5"
        if( Input.GetKeyDown(KeyCode.Keypad5) || Input.GetKeyDown(KeyCode.Alpha5)) 
        {
            ButClick("5");        }
        // Вызываем обработчик кнопки "6"
        if( Input.GetKeyDown(KeyCode.Keypad6) || Input.GetKeyDown(KeyCode.Alpha6)) 
        {
             ButClick("6");        }
        // Вызываем обработчик кнопки "7"
        if( Input.GetKeyDown(KeyCode.Keypad7) || Input.GetKeyDown(KeyCode.Alpha7)) 
        {
            ButClick("7");        }
        // Вызываем обработчик кнопки "8"
        if( Input.GetKeyDown(KeyCode.Keypad8) || Input.GetKeyDown(KeyCode.Alpha8)) 
        {
            ButClick("8");        }
        // Вызываем обработчик кнопки "9"
        if( Input.GetKeyDown(KeyCode.Keypad9) || Input.GetKeyDown(KeyCode.Alpha9)) 
        {
            ButClick("9");        }
        // Вызываем обработчик кнопки "-"
        if( Input.GetKeyDown(KeyCode.KeypadMinus) || Input.GetKeyDown(KeyCode.Minus)) 
        {
            ButClick("-");      
        }
        // Вызываем обработчик кнопки "backspace"
        if( Input.GetKeyDown( KeyCode.Backspace ) ) 
        {
            DelClick();
        }
    }

    public void ButClick(string buttontext)
    {

        if(text.text.Length < 3)
        {
            if(buttontext.Equals("-"))
            {
                if(text.text.Length < 1)
                {
                    text.text += buttontext;
                    newtext = text.text;
                }
            }
            else
            {
                text.text += buttontext;
                newtext = text.text;
            }
             
        }

    }

    public void DelClick()
    {
        if (text.text.Length >= 1)
        {
            text.text = text.text.Remove(text.text.Length - 1);
            newtext = text.text;
        }
            
    }
}
