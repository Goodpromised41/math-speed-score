using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Score : MonoBehaviour
{
    public uint score;
    public DateTime dateTime;
    private string nameGamer;
    public string NameGamer
    {
        get => nameGamer;
        set
        {
            if (value.Length > 15)
                nameGamer = value.Substring(0, 15) + "...";
            else
                nameGamer = value;
        }
    }
    public int difficulty;

    public Score(string name, int difficulty, DateTime dateTime, uint score)
    {
        NameGamer = name;
        this.difficulty = difficulty;
        this.dateTime = dateTime;
        this.score = score;
    }
}
